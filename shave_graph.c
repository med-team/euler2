/***************************************************************************
 * Title:          shave_graph.c
 * Author:         Haixu Tang
 * Created:        Jun. 2002
 * Last modified:  May. 2004
 *
 * Copyright (c) 2001-2004 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include <stdinc.h>
#include <extvab.h>
#include <extfunc.h>

#define MIN_LENGTH 1000
#define MIN_MULTIP 10


int realmultip1(EDGE *edge, int l);
int realmultip2(EDGE *edge, int l);
int merge_graph(NODES **vertex, int num_vertex);
int shave_graph(NODES **vertex, int num_vertex, int *chim, int *num_chim);
EDGE *merge_vertex(NODES *vertex);
int chklist(int *list, int n, int index);
EDGE *new_edge(NODES *vertex, NODES *begin, NODES *end, EDGE *edge1, EDGE *edge2, int *beginlist, int *endlist,
		int nbegin, int nend);
void combine_readinterval(EDGE *edge1, EDGE *edge2, EDGE *newedge, int *beginlist, int *endlist, int nbegin, int nend);
int findreadposition(READINTERVAL *readinterval, int num, int readindex, int readposition);
int findreadposition2(READINTERVAL *readinterval, int num, int readindex, int readposition);

int shave_graph(NODES **vertex, int num_vertex, int *chim, int *num_chim)
{
	int	i, j, k, l, n, m, n1, reads;
	int	tot_edge;
	int	nsh, nbul, nch;
	int	maxmlt, maxl, maxk, multip;
	int	true_multip;
	NODES	*begin, *end, *bal_node;
	EDGE	*edge, *edge1, *edge2, *bal_edge, *bal_edge1, *bal_edge2;

/*	Remove bulges and shave edges linking sources & sinks	*/

	nsh = nbul = nch = 0;
	do	{
		m = 0;
		for(i = 0; i < num_vertex; i ++)	{
			for(k = 0; k < vertex[i] -> num_nextedge; k ++)	{
				edge1 = vertex[i] -> nextedge[k];
				bal_edge1 = edge1 -> bal_edge;
				j = k + 1;
				while(j < vertex[i] -> num_nextedge)	{
					edge2 = vertex[i] -> nextedge[j];
					bal_edge2 = edge2 -> bal_edge;
					if(edge2 -> end == edge1 -> end && edge1 != bal_edge2 &&
					   abs(edge1 -> length - edge2 -> length) < MIN_INT)	{
						movereadinterval(edge1, edge2);
						erasedge(edge2);
						if(bal_edge2 != edge2)	{
							movereadinterval(bal_edge1, bal_edge2);
							erasedge(bal_edge2);
							m ++;
						}
						m ++;
					} else	{
						j ++;
					}
				}
			}
		}
		nbul += m;

/*	Remove end edges	*/

		n = 0;
		for(i = 0; i < num_vertex; i ++)	{
			if(vertex[i] -> num_lastedge != 0)	continue;
			j = 0;
			while(j < vertex[i] -> num_nextedge)	{
				edge = vertex[i] -> nextedge[j];
				multip = realmultip1(edge, 100);
				if(edge -> end -> num_lastedge > 1 && multip <= LOW_COV)	{
					bal_edge = edge -> bal_edge;
					erasedge(edge);
					if(edge != bal_edge)	{
						erasedge(bal_edge);
						n ++;
					}
					n ++;
				} else	{
					j ++;
				}
			}
		}
		nsh += n;
		num_vertex = merge_graph(vertex, num_vertex);
	} while(n > 0 || m > 0);

/*	Erase chimeric edges	*/

	do	{
		n1 = 0;
		for(i = 0; i < num_vertex; i ++)	{
			j = 0;
			while(j < vertex[i] -> num_nextedge)	{
				edge = vertex[i] -> nextedge[j];
				if(vertex[i] -> num_nextedge > 1 &&
				   edge -> end -> num_lastedge > 1 && edge -> length < MIN_LENGTH &&
				   edge -> multip < MIN_MULTIP)	{
					multip = realmultip2(edge, overlaplen);
					if(multip <= MID_COV)	{
						bal_edge = edge -> bal_edge;
						for(m = 0; m < edge -> multip; m ++)	{
							reads = edge -> readinterval[m].eq_read;
							chim[nch ++] = reads;
							n1 ++;
						}
						erasedge(edge);
						if(edge != bal_edge)	{
							for(m = 0; m < bal_edge -> multip; m ++)	{
								reads = bal_edge -> readinterval[m].eq_read;
								chim[nch ++] = reads;
								n1 ++;
							}
							erasedge(bal_edge);
						}
					} else	{
						j ++;
					}
				} else	{
					j ++;
				}
			}
		}
		num_vertex = merge_graph(vertex, num_vertex);

		m = 0;
		for(i = 0; i < num_vertex; i ++)	{
			for(k = 0; k < vertex[i] -> num_nextedge; k ++)	{
				edge1 = vertex[i] -> nextedge[k];
				bal_edge1 = edge1 -> bal_edge;
				j = k + 1;
				while(j < vertex[i] -> num_nextedge)	{
					edge2 = vertex[i] -> nextedge[j];
					bal_edge2 = edge2 -> bal_edge;
					if(edge2 -> end == edge1 -> end && edge1 != bal_edge2 &&
					   abs(edge1 -> length - edge2 -> length) < MIN_INT)	{
						movereadinterval(edge1, edge2);
						erasedge(edge2);
						if(bal_edge2 != edge2)	{
							movereadinterval(bal_edge1, bal_edge2);
							erasedge(bal_edge2);
							m ++;
						}
						m ++;
					} else	{
						j ++;
					}
				}
			}
		}
		nbul += m;

/*	Remove end edges	*/

		n = 0;
		for(i = 0; i < num_vertex; i ++)	{
			if(vertex[i] -> num_lastedge != 0)	continue;
			j = 0;
			while(j < vertex[i] -> num_nextedge)	{
				edge = vertex[i] -> nextedge[j];
				multip = realmultip1(edge, END_LEG);
				if(edge -> end -> num_lastedge > 1 && multip <= LOW_COV)	{
					bal_edge = edge -> bal_edge;
					erasedge(edge);
					if(edge != bal_edge)	{
						erasedge(bal_edge);
						n ++;
					}
					n ++;
				} else	{
					j ++;
				}
			}
		}
		nsh += n;
		num_vertex = merge_graph(vertex, num_vertex);

		for(i = 0; i < num_vertex; i ++)	{
			bal_node = vertex[i] -> bal_node;
			if(vertex[i] -> num_lastedge != bal_node -> num_nextedge ||
			   vertex[i] -> num_nextedge != bal_node -> num_lastedge)	{
				printf("pos2 vertex %d(%d-%d) bal_node %d(%d-%d)\n",
					vertex[i], vertex[i] -> num_lastedge, vertex[i] -> num_nextedge,
					bal_node, bal_node -> num_lastedge, bal_node -> num_nextedge);
				getchar();
			}
		}
	} while(n1 > 0);

	tot_edge = 0;
	for(i = 0; i < num_vertex; i ++)	{
		tot_edge += vertex[i] -> num_nextedge;
	}
	printf("%d short end edges, %d bulges and %d chimeric edges shaved, %d vertices %d edges left.\n",
		nsh, nbul, nch, num_vertex, tot_edge);

	*num_chim = nch;
	return(num_vertex);
}

int realmultip1(EDGE *edge, int l)
{
	int	i, j, k, k1, k2;

	if(edge -> length < l)	return(0);
	k = 0;
	for(i = 0; i < edge -> multip; i ++)	{
		if(edge -> readinterval[i].length >= l)	{
			k ++;
		}
	}
	return(k);
}

int realmultip2(EDGE *edge, int l)
{
	int	i, j, k1, k2, k;

	k = 0;
	for(i = 0; i < edge -> multip; i ++)	{
		if(edge -> readinterval[i].length >= min(l, edge -> length))	{
			k ++;
		}
	}
	return(k);
}


int merge_graph(NODES **vertex, int num_vertex)
{
	int	i, j, k, l;
	int	num_vertex_old;
	NODES	*bal_node;
	READPOSITION	*readposition;
	EDGE	*edge1, *edge2;

	do	{
		num_vertex_old = num_vertex;
		i = 0;
		while(i < num_vertex)	{
			if(vertex[i] -> num_nextedge == 0 && vertex[i] -> num_lastedge == 0)	{
				free_nodes(vertex[i]);
				for(j = i; j < num_vertex - 1; j ++)	{
					vertex[j] = vertex[j + 1];
				}
				num_vertex --;
			} else if(vertex[i] -> num_nextedge == 1 && vertex[i] -> num_lastedge == 1 &&
				  vertex[i] -> nextedge[0] != vertex[i] -> lastedge[0]) {
				bal_node = vertex[i] -> bal_node;
				edge1 = merge_vertex(vertex[i]);
				if(bal_node != vertex[i])	{
					if(bal_node -> num_lastedge == 1 && bal_node -> num_nextedge == 1)	{
						if(bal_node == edge1 -> end || bal_node == edge1 -> begin)	{
							edge2 = merge_vertex(bal_node);
							edge2 -> bal_edge = edge2;
						} else	{
							edge2 = merge_vertex(bal_node);
							edge1 -> bal_edge = edge2;
							edge2 -> bal_edge = edge1;
						}
					} else	{
						printf("pos3 vertex %d(%d-%d) bal_node %d(%d-%d)\n",
							vertex[i], vertex[i] -> num_lastedge, vertex[i] -> num_nextedge,
							bal_node, bal_node -> num_lastedge, bal_node -> num_nextedge);
						exit(0);
					}
				} else	{
					edge1 -> bal_edge = edge1;
				}
			} else	{
				i ++;
			}
		}
	} while(num_vertex_old > num_vertex);
	return(num_vertex);
}

EDGE *merge_vertex(NODES *vertex)
{
	int	i, j, k, l, m, n, k1, k2, n1, n2, c, q, p, num;
	NODES	*begin, *end, *b0, *e0;
	int	*beginlist, *endlist;
	int	*del_path;
	EDGE	*lastedge, *nextedge, *newedge, *new1edge;
	double	v, r;

	begin = vertex -> lastedge[0] -> begin;
	end = vertex -> nextedge[0] -> end;
	lastedge = vertex -> lastedge[0];
	nextedge = vertex -> nextedge[0];
	if(lastedge == nextedge)	{
		return(lastedge);
	}
	newedge = new_edge(vertex, begin, end, lastedge, nextedge, beginlist, endlist, 0, 0);
	erasedge(lastedge);
	erasedge(nextedge);
	return(newedge);
}

EDGE *new_edge(NODES *vertex, NODES *begin, NODES *end, EDGE *edge1, EDGE *edge2, int *beginlist, int *endlist,
		int nbegin, int nend)
{
	int	i, j, k, l, m;
	EDGE	**tmpedge, *edge;

	edge = (EDGE *) ckalloc(1 * sizeof(EDGE));
	edge -> begin = begin;
	edge -> end = end;
	edge -> length = edge1 -> length + edge2 -> length - VERTEX_SIZE;
	combine_readinterval(edge1, edge2, edge, beginlist, endlist, nbegin, nend);

	tmpedge = (EDGE **) ckalloc(MAX_BRA * sizeof(EDGE *));

	for(i = 0; i < begin -> num_nextedge; i ++)	{
		tmpedge[i] = begin -> nextedge[i];
	}
	free((void **) begin -> nextedge);
	begin -> nextedge = (EDGE **) ckalloc((begin -> num_nextedge + 1) * sizeof(EDGE *));
	for(i = 0; i < begin -> num_nextedge; i ++)	{
		begin -> nextedge[i] = tmpedge[i];
	}
	begin -> nextedge[i] = edge;
	begin -> num_nextedge ++;

	for(i = 0; i < end -> num_lastedge; i ++)	{
		tmpedge[i] = end -> lastedge[i];
	}
	free((void **) end -> lastedge);
	end -> lastedge = (EDGE **) ckalloc((end -> num_lastedge + 1) * sizeof(EDGE *));
	for(i = 0; i < end -> num_lastedge; i ++)	{
		end -> lastedge[i] = tmpedge[i];
	}
	end -> lastedge[i] = edge;
	end -> num_lastedge ++;

	free((void **) tmpedge);
	return(edge);
}

void combine_readinterval(EDGE *edge1, EDGE *edge2, EDGE *newedge, int *beginlist, int *endlist, int nbegin, int nend)
{
	int	i, j, k, l, m, n, c, num1, t, q;
	int	reads;
	char	*mark1, *mark2;
	double	rm;
	NODES	*vertex0;
	READINTERVAL *readinterval;
	EDGE	*edge01, *edge02, *edge3;

	vertex0 = edge1 -> end;

	l = edge1 -> length - VERTEX_SIZE;
	k = edge1 -> multip + edge2 -> multip;
	mark1 = (char *) ckalloc(k * sizeof(char));
	mark2 = (char *) ckalloc(k * sizeof(char));
	readinterval = (READINTERVAL *) ckalloc(k * sizeof(READINTERVAL));
	newedge -> readinterval = (READINTERVAL *) ckalloc(k * sizeof(READINTERVAL));
	vertex0 = edge1 -> end;
	n = 0;
	for(i = 0; i < edge2 -> multip; i ++)	{
		c = findreadposition(edge1 -> readinterval, edge1 -> multip, edge2 -> readinterval[i].eq_read,
				 edge2 -> readinterval[i].begin);
		if(c >= 0)	{
			newedge -> readinterval[n].eq_read = edge1 -> readinterval[c].eq_read;
			newedge -> readinterval[n].offset = edge1 -> readinterval[c].offset;
			newedge -> readinterval[n].begin = edge1 -> readinterval[c].begin;
			newedge -> readinterval[n].length = edge1 -> readinterval[c].length +
							 edge2 -> readinterval[i].length - VERTEX_SIZE;
			mark1[c] = 1;
			mark2[i] = 1;
			n ++;
		} else	{
			c = -1;
			for(m = 0; m < vertex0 -> num_lastedge; m ++)	{
				if(vertex0 -> lastedge[m] == edge1)	continue;
				edge3 = vertex0 -> lastedge[m];
				c = findreadposition(edge3 -> readinterval, edge3 -> multip,
					 edge2 -> readinterval[i].eq_read,
					 edge2 -> readinterval[i].begin);
				if(c >= 0)	break;
			}
			k = chklist(beginlist, nbegin, edge2 -> readinterval[i].eq_read);
			if(vertex0 -> num_lastedge == 1 || c < 0 && k < 0)	{
				newedge -> readinterval[n].eq_read = edge2 -> readinterval[i].eq_read;
				newedge -> readinterval[n].offset = edge2 -> readinterval[i].offset + l;
				newedge -> readinterval[n].begin = edge2 -> readinterval[i].begin;
				newedge -> readinterval[n].length = edge2 -> readinterval[i].length;
				if(edge2 -> readinterval[i].offset + edge2 -> readinterval[i].length >= edge2 -> length) {
					mark2[i] = 1;
				}
				n ++;
			}
		}
	}

	for(i = 0; i < edge1 -> multip; i ++)	{
		if(mark1[i] == 1)	continue;
		c = -1;
		for(m = 0; m < vertex0 -> num_nextedge; m ++)	{
			if(vertex0 -> nextedge[m] == edge2)	continue;
			edge3 = vertex0 -> nextedge[m];
			c = findreadposition2(edge3 -> readinterval, edge3 -> multip, edge1 -> readinterval[i].eq_read,
				 edge1 -> readinterval[i].begin + edge1 -> readinterval[i].length - VERTEX_SIZE);
			if(c >= 0)	break;
		}
		k = chklist(endlist, nend, edge1 -> readinterval[i].eq_read);
		if(vertex0 -> num_nextedge == 1 || c < 0 && k < 0)	{
			newedge -> readinterval[n].eq_read = edge1 -> readinterval[i].eq_read;
			newedge -> readinterval[n].offset = edge1 -> readinterval[i].offset;
			newedge -> readinterval[n].begin = edge1 -> readinterval[i].begin;
			newedge -> readinterval[n].length = edge1 -> readinterval[i].length;
			if(edge1 -> readinterval[i].offset == 0)	{
				mark1[i] = 1;
			}
			n ++;
		}
	}
	newedge -> multip = n;
	k = 0;
	for(i = 0; i < edge1 -> multip; i ++)	{
		if(!mark1[i])	{
			readinterval[k ++] = edge1 -> readinterval[i];
		}
	}
	edge1 -> multip = k;
	for(i = 0; i < edge1 -> multip; i ++)	{
		edge1 -> readinterval[i] = readinterval[i];
	}
	k = 0;
	for(i = 0; i < edge2 -> multip; i ++)	{
		if(!mark2[i])	{
			readinterval[k ++] = edge2 -> readinterval[i];
		}
	}
	edge2 -> multip = k;
	for(i = 0; i < edge2 -> multip; i ++)	{
		edge2 -> readinterval[i] = readinterval[i];
	}
	free((void *) readinterval);
	free((void *) mark1);
	free((void *) mark2);
}

int chklist(int *list, int n, int index)
{
	int	i, j, k, l;
	
	for(i = 0; i < n; i ++)	{
		if(list[i] == index)	return(i);
	}
	return(-1);
}

int findreadposition(READINTERVAL *readinterval, int num, int readindex, int readposition)
{
	int	i, j, k, l;

	for(i = 0; i < num; i ++)	{
		if(readinterval[i].eq_read == readindex &&
		   readinterval[i].begin + readinterval[i].length - VERTEX_SIZE == readposition)	{
			return(i);
		}
	}
	return(-1);
}

int findreadposition2(READINTERVAL *readinterval, int num, int readindex, int readposition)
{
	int	i, j, k, l;

	for(i = 0; i < num; i ++)	{
		if(readinterval[i].eq_read == readindex && readinterval[i].begin == readposition)	{
			return(i);
		}
	}
	return(-1);
}
