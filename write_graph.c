/***************************************************************************
 * Title:          write_graph.c
 * Author:         Haixu Tang
 * Created:        Jun. 2002
 * Last modified:  May. 2004
 *
 * Copyright (c) 2001-2004 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include <stdinc.h>
#include <extvab.h>
#include <extfunc.h>

void write_graph(NODES **vertex, int num_vertex, FILE *fp, FILE *fp1);

void write_graph(NODES **vertex, int num_vertex, FILE *fp, FILE *fp1)
{
	int	i, j, k, l, n;
	char	temp[100];
	EDGE	*edge;

	n = 0;
	for(i = 0; i < num_vertex; i ++)	{
		for(j = 0; j < vertex[i] -> num_nextedge; j ++)	{
			edge = vertex[i] -> nextedge[j];
			sprintf(temp, "edge%d", n + 1);
			writeseq(fp, edge -> seq, temp, edge -> length);
			edge -> start_cover = n;
			n ++;
		}
	}

	fprintf(fp1, "Number_of_Vertex %d\n", num_vertex);
	for(i = 0; i < num_vertex; i ++)	{
		fprintf(fp1, "Vertex %d %d %d\n", i, vertex[i] -> num_nextedge, vertex[i] -> num_lastedge);
		fprintf(fp1, "Last_edge ");
		for(j = 0; j < vertex[i] -> num_lastedge; j ++)	{
			fprintf(fp1, "%d %d ", vertex[i] -> lastedge[j] -> start_cover, vertex[i] -> lastedge[j] -> bal_edge -> start_cover);
		}
		fprintf(fp1, "\n");
		fprintf(fp1, "Next_edge ");
		for(j = 0; j < vertex[i] -> num_nextedge; j ++)	{
			fprintf(fp1, "%d %d ", vertex[i] -> nextedge[j] -> start_cover, vertex[i] -> nextedge[j] -> bal_edge -> start_cover);
		}
		fprintf(fp1, "\n");
	}
}
