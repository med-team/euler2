/***************************************************************************
 * Title:          eraseedge.c
 * Author:         Haixu Tang
 * Created:        Jun. 2002
 * Last modified:  May. 2004
 *
 * Copyright (c) 2001-2004 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/

#include <stdinc.h>
#include <extvab.h>
#include <extfunc.h>

void erasedge(EDGE *edge);
int crossedge(EDGE *edge, PATH *path);
int erasecycle(NODES **vertex, int num_vertex);

void erasedge(EDGE *edge)
{
	int	i, j, k, l, n, m;
	NODES	*begin, *end;

	begin = edge -> begin;
	end = edge -> end;
	k = searcherase(begin -> nextedge, edge, begin -> num_nextedge);
	erasenext(begin, k);
	k = searcherase(end -> lastedge, edge, end -> num_lastedge);
	eraselast(end, k);

	free((void *) edge -> readinterval);
	free((void *) edge);
}

int crossedge(EDGE *edge, PATH *path)
{
	int	i, j, k, l, n1, n2;
	NODES	*begin, *end;

	n1 = 0;
	n2 = 0;
	begin = edge -> begin;
	for(i = 0; i < begin -> num_path; i ++)	{
		j = begin -> path_index[i];
		k = begin -> path_pos[i];
		if(k < path[j].len_path && path[j].edge[k] == edge)	{
			if(k > 0 || begin -> num_lastedge == 0)	{
				n1 ++;
			}
			if(k < path[j].len_path - 1 || edge -> end -> num_nextedge == 0)	{
				n2 ++;
			}
		}
	}

	if(n1 == 0 || n2 == 0)	{
		return(0);
	} else	{
		return(1);
	}
}

int erasecycle(NODES **vertex, int num_vertex)
{
	int	i, j, k;
	EDGE	*edge;

	for(i = 0; i < num_vertex; i ++)	{
		if(vertex[i] -> num_nextedge == 1 && vertex[i] -> num_lastedge == 1)	{
			edge = vertex[i] -> nextedge[0];
			if(edge -> begin == edge -> end && edge -> length < 1000)	{
				erasedge(edge);
			}
		}
	}
	num_vertex = merge_graph(vertex, num_vertex);
	return(num_vertex);
}
