/***************************************************************************
 * Title:          eqtrans_bal.c
 * Author:         Haixu Tang
 * Created:        Jun. 2002
 * Last modified:  May. 2004
 *
 * Copyright (c) 2001-2004 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/

#include <stdinc.h>
#include <extvab.h>
#include <extfunc.h>

int nsuper, *numtangle, *maxmultip, *maxlength, *avelength, *mlength;

int eqtrans_bal(NODES **vertex, int num_vertex, PATH *path, int num_path, int num_seq);

int eqtrans_bal(NODES **vertex, int num_vertex, PATH *path, int num_path, int num_seq)
{
	int	i, j, k, l, n, m;
	int	tot_edge, tot_edge_old, tot_path, tot_path_old;
	int	**num_pa;
	int	votethresh;
	EDGE	*edge, **all_edge, *edge1, *edge2, *edge0;

	votethresh = LOW_COV;

	num_pa = (int **) ckalloc(MAX_BRA * sizeof(int *));
	for(i = 0; i < MAX_BRA; i ++)	{
		num_pa[i] = (int *) ckalloc(MAX_BRA * sizeof(int));
	}

	tot_edge = count_edge_simp(vertex, num_vertex, num_pa);

	do	{
		tot_edge_old = tot_edge;
		tot_path_old = tot_path;
		num_vertex = vertex_run(vertex, num_vertex, path, num_path, votethresh, num_seq);
		tot_edge = count_edge_simp(vertex, num_vertex, num_pa);
		tot_path = count_path(path, num_path);
	} while(tot_edge_old > tot_edge || tot_path_old > tot_path);

	num_vertex = splitbeg(vertex, num_vertex, path, num_path);
	num_vertex = merge_graph_path(vertex, num_vertex, path, num_path, votethresh);
	tot_edge = count_edge_simp(vertex, num_vertex, num_pa);

	for(i = 0; i < MAX_BRA; i ++)	{
		free((void *) num_pa[i]);
	}
	free((void **) num_pa);
	return(num_vertex);
}
