/***************************************************************************
 * Title:          locpath.c
 * Author:         Haixu Tang
 * Created:        Jun. 2002
 * Last modified:  May. 2004
 *
 * Copyright (c) 2001-2004 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include <stdinc.h>
#include <extvab.h>
#include <extfunc.h>

#define MAX_COPY 100

int locpath(EDGE *edge1, EDGE *edge2, int min_leg, int max_leg, int len, PATH *path);

int locpath(EDGE *edge1, EDGE *edge2, int min_leg, int max_leg, int len, PATH *path)
{
	int	i, j, k, l, num_path, n;
	NODES	*vertex;
	EDGE	*edge;
	PATH	*path_temp;

	if(len > max_leg)	{
		return(0);
	}

	vertex = edge1 -> end;

	path_temp = (PATH *) ckalloc(vertex -> num_nextedge * sizeof(PATH));
	for(i = 0; i < vertex -> num_nextedge; i ++)	{
		path_temp[i].edge = (EDGE **) ckalloc(max_leg * sizeof(EDGE *));
	}
	num_path = 0;
	for(i = 0; i < vertex -> num_nextedge; i ++)	{
		edge = vertex -> nextedge[i];
		if(edge -> start_cover > MAX_COPY)	continue;
		edge -> start_cover ++;

		path_temp[i].edge[0] = edge;
		path_temp[i].len_path = 1;
		if(edge == edge2 && len >= min_leg)	{
			num_path ++;
			k = i;
		}
		n = locpath(edge, edge2, min_leg, max_leg, len + edge -> length - VERTEX_SIZE, &path_temp[i]);
		if(n == 1)	{
			k = i;
		}
		num_path += n;
		if(num_path > 1)	{
			break;
		}
	}

	if(num_path == 1)	{
		for(i = 0; i < path_temp[k].len_path; i ++)	{
			path -> edge[path -> len_path + i] = path_temp[k].edge[i];
		}
		path -> len_path += path_temp[k].len_path;
	}

	for(i = 0; i < vertex -> num_nextedge; i ++)	{
		free((void **) path_temp[i].edge);
	}
	free((void *) path_temp);
	return(num_path);
}
