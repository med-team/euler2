/***************************************************************************
 * Title:          chk_consist.c
 * Author:         Haixu Tang
 * Created:        Jun. 2002
 * Last modified:  May. 2004
 *
 * Copyright (c) 2001-2004 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/

#include <stdinc.h>
#include <extvab.h>
#include <extfunc.h>

int chk_consist(PATH *startpath, PATH *midpath, int num_midpath, int *n);

int chk_consist(PATH *startpath, PATH *midpath, int num_midpath, int *n)
{
	int	i, j, k, l, c;

	c = -1;
	for(i = 0; i < num_midpath; i ++)	{
		if(startpath -> len_path <= midpath[i].len_path)	{
			for(j = 0; j < startpath -> len_path; j ++)	{
				if(startpath -> edge[j] != midpath[i].edge[j])	{
					break;
				}
			}
			if(j == startpath -> len_path)	{
				c = 0;
				*n = i;
				break;
			}
		} else	{
			for(j = 0; j < midpath[i].len_path; j ++)	{
				if(startpath -> edge[j] != midpath[i].edge[j])	{
					break;
				}
			}
			if(j == midpath[i].len_path)	{
				c = i + 1;
				break;
			}
		}
	}

	return(c);
}
