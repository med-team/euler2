/***************************************************************************
 * Title:          param.h
 * Author:         Haixu Tang
 * Created:        Jun. 2002
 * Last modified:  May. 2004
 *
 * Copyright (c) 2001-2004 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
#include <nuc.h>
#include <weight.h>

int idum;
char END_MERGE;
int LOW_COV, LOW_COV_PATH, MID_COV, SHORT_D;
int MAX_TMP_LEG;
int MIN_END, MIN_INT, END_LEG, INT_LEG;
int MAX_BRA;
int max_seq;
int match_score, mov_qual;
double MIN_PERC, MIN_IDENTITY;
int MIN_OVERLAP;
int start_ent, end_ent, band;
int SHORTEST_OVERLAP;
int gap_k;
int g, h;
int overlaplen, min_numcov;
int MIN_OVERLAP2, MIN_END2;
int MAX_NODES, MAX_EDGE;
int SMALL_CYCLE;
long n_ban;
int word_len;
int MAX_DIF;
int LPAT;
int MIS_SCORE;
int LINK_COV, LINK_MIN_LEN;
int VERTEX_SIZE;
int EndLength;
int BulgeLength, BulgeCoverage, WhirlLength,
  ChimericTerm, ChimericCoverage, ErosionLength,
  SecondChimericCoverage;
int SMALL_EDGE;
int FILTER_THRESH_ABSOLUTE;
double FILTER_THRESH_PERC;
